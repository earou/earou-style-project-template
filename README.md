# To Assignment Reviewer

1. I followed the step1-6 to implement this assignment. And tag release after
    finishing each steps. Development milestones can be checked by switching to tag branches. 
2. Concept of build version and artifacts management have been implemented.
3. I also add ci pipeline according to my previous experience. 
4. Option `-h` is available for all shell scripts to show usage of the script.

# Project

**Project Code:** counter-api-service

* [Dev Environment](#dev-environment)
* [Project Structure](#project-structure)
* [How To Run](#how-to-run)
* [How To Test](#how-to-test)
* [Packing and delivery](#packing-and-delivery)
* [Others](#others)

### Release Notes:
* rel-v1.0.0-step6
    * a script `wait_counter.sh` that will wait for all counters to expire.
* rel-v0.0.5-step5
    * support delete counter by restful api.   
* rel-v0.0.4-step4
    * `setup_api.sh` support dynamically change the number of backend server.  
* rel-v0.0.3-step3
    * support list all counters.  
* rel-v0.0.2-step2
    * create and check a counter by POST and GET method.  
* rel-v0.0.1_step1
    * `setup_api.sh` script for provisioning container instances of counter-api and a proxy for L7 load balancing.  

# Dev Environment

### OS:
- Linux
- MacOS

### Prerequisites:
- Python 2.7
- pip
- Bash Shell
- Docker CE 17 or above (https://store.docker.com/search?offering=community&type=edition)

# Project Structure

```
counter-api-service
    ├── counter-api
    │   ├── doc (design document)
    │   ├── src (python project)
    │   ├── Dockerfile
    │   ├── 01_test.sh (PyLint and unit tests)
    │   ├── 02_build.sh (docker build with build version)
    │   └── README.md
    ├── ngx
    │   └── (L7 proxy nginx configs)
    ├── tests (for integration test)
    │   ├── testcases
    │   └── run_tests.sh
    ├── pipeline
    │   └── (for continuous integration)
    ├── setup_api.sh
    ├── terminate_api.sh
    ├── create_counter.sh
    ├── list_counter.sh
    ├── wait_counter.sh
    ├── version (text file include version info)
    └── README.md
```

# How to Run
### 1. build docker images and pack operation scripts for current changes. 
```
cd <project-folder>/pipeline
./01_build-artifacts.sh
```
### 2. setup counter-api service by latest version
```
cd <project-folder>/artifacts/latest
./setup_api.sh 3
```
### 3. using `create_counter.sh` to create a great amount of counters.
create 7 counters expired after 1000 secs.
```
cd <project-folder>/artifacts/latest
./create_counter.sh 7 1000
```
### 4. using `list_counter.sh` to show status of all counters.
```
cd <project-folder>/artifacts/latest
./list_counter.sh
```
### 5. terminate counter-api service
```
cd <project-folder>/artifacts/latest
./terminate_api.sh
```
### Optional: you can also run script instantly from source code when developing.
```
cd <project-folder>/
./setup_api.sh 3
```

# How to Test
### run all tests in this project for latest version.
```
cd <project-folder>/pipeline
./02_ci-tests.sh
```
### Optional: you can also run script instantly from source code when developing.
Notes: the following test script do not cover codes under python project:`counter-api`
```
cd <project-folder>/tests/
./run_tests.sh
```
## Playground !!!
```
# going to latest build folder.
cd <project-folder>/artifacts/latest

# setup service.
./setup_api.sh 3

# check hostname and build version.
curl http://127.0.0.1/
curl http://127.0.0.1/

# create 20 counters expire 5 mins later.
./create_counter.sh 20 300

# list all counters uuid and pick one.
curl http://127.0.0.1/counter/

# check single counter status.
curl http://127.0.0.1/counter/${uuid}/

# you can change the number of backend instance.
./setup_api.sh 5

# check if docker instance changed.
docker ps

# you can still check status of all counter.
./list_counter.sh

# try wait_counter.sh
./wait_counter.sh

# terminate everything before you leave 
./terminate_api.sh
```

# Packing and delivery

If you have executed the pipeline scripts. There should be `version.tar.gz`.
`version.tar.gz` include operation scripts. you can deliver it to anywhere you want.

For the docker image. just tag and push to the docker registry you want.

# Others
- [Developer Guide for earou/counter-api](counter-api/README.md)
