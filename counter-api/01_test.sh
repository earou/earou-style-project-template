#!/bin/bash
set -e
# Doc
function arg_doc {
  echo -e "\033[1;37mDescription:\033[m"
  echo "    execute pylint and unit tests of python project."
  echo ""
  echo -e "\033[1;37mUsage: $0 \033[m"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi

# switch current directory to python source code directory
cd src

# pylint test
echo -e "\033[1;37m  *  Run PyLint:\033[m"
docker run -v "$(pwd):/code" eeacms/pylint

# unit test
echo -e "\033[1;37m  *  Run Unit Test:\033[m"
docker run -v $(pwd):/app earou/api-counter python -m unittest discover tests

echo -e "\033[1;37m  *  Test succeeded!!\033[m"