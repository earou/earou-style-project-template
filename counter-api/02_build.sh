#!/bin/bash
set -e

# Args
BUILD_ID=$1
# Contants
REPO_NAME='earou/api-counter'
# Doc
function arg_doc {
  echo -e "\033[1;37mDescription:\033[m"
  echo "    build docker image with version tag."
  echo ""
  echo -e "\033[1;37mUsage: $0 <build-id> \033[m"
  echo "  * build-id: version_YYYYmmDD-HHMM"
  echo ""
  echo -e "\033[1;37mExamples:\033[m"
  echo "   1. build docker image with tag: $0 0.0.1_20190115-1905"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi
# Arg Validation
if [ "${BUILD_ID}" == "" ]; then
  arg_doc
  echo -e "\033[1;31mbuild-id is required.\033[m"
  exit 1
fi

echo -e "\033[1;37m  *  Start to build docker image...\033[m"
# pack version info into docker image
echo "$BUILD_ID" > src/version
# docker build
docker build -t ${REPO_NAME}:${BUILD_ID} .
# tag latest
docker tag ${REPO_NAME}:${BUILD_ID} ${REPO_NAME}:latest
# clean tmp file
rm src/version
echo -e "\033[1;37m  *  Build succeeded!!\033[m"