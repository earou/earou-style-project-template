# Description
`counter-api` is a python project which is a restful api server providing countdown counters for user.

# Developer Guide

## Install prerequisite python modules

It's not necessary if running by Docker.
```shell
sudo pip install flask-restful

```

## Run instantly
```shell
cd <project-folder>/counter-api/src
python app.py
```
Check if server is running. Open a new terminal to input commands.
```shell
➜ curl http://127.0.0.1:5000
"N0315D170524.local ver: dev"

```

## Run by Docker
```shell
cd <project-folder>/counter-api

# docker build, change <version> to real value. e.g. 0.0.1_201901151905
./02_build.sh <version>

# check docker image has been created
docker images | grep 'earou/api-counter'

# execute
docker run -p 5000:5000 earou/api-counter:latest

# or execute with specific version
docker run -p 5000:5000 earou/api-counter:<version>
```
Check if server is running. Open a new terminal to input commands.
```shell
➜ curl http://127.0.0.1:5000
"963a727f9dcf ver: 0.0.1_201901151905"

```

## Run lint and unit test
To build a good habit of development. Always run tests before build and code commit.
```shell
cd <project-folder>/counter-api

# run tests
./01_test.sh
```

# Component Design

![Class Diagram](doc/class_diagram.png)

### Counter
* Model Class

### CounterManager
* Controller, manage resources and communicate with the proxy of data persistence.

### Resource Classes
Resource classes are restful api handlers. Method name will be mapping to HTTP methods.
There are four Resource classes:
* ServiceInfo
    - Provide server and app info
* CounterResApi
    - Get counter
* CounterListApi
    - List counter
    - New counter
* CounterStopApi
    - Stop a counter

### Data Persistence Proxy
* CounterStorage
    - Abstract class to define the interfaces.
* CounterFileStorage
    - Simple implementaion
* FakeStorage
    - For unit test

