# -*- coding: utf-8 -*-
"""counter-api

This module is the main entry of counter-api,
it will start a restful server listening port 5000.

Example:
        $ python app.py
"""
from flask import Flask
from flask_restful import Api
from handler.serviceinfo import ServiceInfo
from handler.counterapi import CounterResApi, CounterListApi, CounterStopApi

def start_service():
    """Start the restful server.
    """
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(ServiceInfo, "/")
    api.add_resource(CounterResApi,
                     "/counter/<string:uid>/",
                     "/counter/<string:uid>")
    api.add_resource(CounterListApi,
                     "/counter/",
                     "/counter")
    api.add_resource(CounterStopApi,
                     "/counter/<string:uid>/stop/")

    app.run(host='0.0.0.0')

if __name__ == "__main__":
    start_service()
