# -*- coding: utf-8 -*-
"""countermanager
This module include a controller class, CounterManager.
"""
from model.counter import Counter
from storage.filestore import CounterFileStorage
class CounterManager(object):
    """Controller class of counter api. And it is singleton.

    Attributes:
        counters (dict): A dict of counter instances.
        storage_proxy (CounterStorage): data presistence proxy.
    """
    def __init__(self):
        self.counters = {}
        self.storage_proxy = CounterFileStorage()
    def list_counter(self):
        """list all counter's uuid in storage.
        Returns:
            list: uuid list of counters in storage.
        """
        return self.storage_proxy.list_counter()
    def get_counter(self, uid):
        """Get counter from counters and try to
            load from data storage if not found.
        Args:
            uid (str): get counter by uid.
        Returns:
            Counter: return a existed counter. or None.
        """
        if self.counters.has_key(uid):
            if self.counters[uid].is_finished():
                self.remove_counter(uid)
                return None
            return self.counters[uid]
        return self.__load_counter(uid)

    def new_counter(self, end_num):
        """New counter and put into counters.
           and data storage.
        Args:
            end_num (int): Specify this counter will finish
            after end_num seconds.
        Returns:
            Counter: A new counter.
        """
        try:
            new_counter = Counter(end_num)
            self.counters[new_counter.uid] = new_counter
            self.storage_proxy.save_counter(new_counter)
            return new_counter
        except ValueError:
            pass
        except TypeError:
            pass
        return None
    def remove_counter(self, uid):
        """Remove counter from counters.
           and data storage.
        Args:
            uid (str): remove the counter by uid.
        """
        if self.counters.has_key(uid):
            self.counters.pop(uid)
        self.storage_proxy.remove_counter(uid)
    def __load_counter(self, uid):
        """Load counter from data storage.
           and put into counters.
        Args:
            uid (str): load counter by uid.
        Returns:
            Counter: If there is no data from
                data storage, return None.
        """
        stored_counter = self.storage_proxy.load_counter(uid)
        if stored_counter is not None:
            self.counters[stored_counter.uid] = stored_counter
        return stored_counter

counter_manager = CounterManager()
