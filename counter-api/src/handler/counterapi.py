# -*- coding: utf-8 -*-
"""counterapi

This module define classes to response counter resource for a restful api call.
"""
from flask_restful import Resource
from flask import request
from flask import make_response
from control.countermanager import counter_manager
class CounterResApi(Resource):
    """CounterResApi is a restful request handler
           for an existed counter resource.
       Provide following methods:
           GET - return a existed counter status.
                 Example: {"current", 3, "to": 10}
    """
    def get(self, uid):
        """Get counter status.
        Args:
            uid (str): get counter by uid.
        Returns:
            json: current count and to.
        """
        existed_counter = counter_manager.get_counter(uid)
        if existed_counter is not None:
            cur = existed_counter.get_current_num()
            to = existed_counter.end_num
            return {"current": cur, "to": to}, 200
        return 'counter not found or expired.', 404

class CounterListApi(Resource):
    """CounterListApi is a restful request handler
           for query and new counter resource.
       Provide following methods:
           LIST - return a counter list.
           POST - new a counter and return its uniq id.
    """
    def get(self):
        """Reture a list of counter's uuid.
            counters in the list might be expired.
        Returns:
            list: list of counter's uuid.
        """
        counter_list = counter_manager.list_counter()
        msg = ""
        for uid in counter_list:
            msg = msg + uid + '\n'
        resp = make_response(msg, 200)
        resp.mimetype = 'text/plain'
        return resp
    def post(self):
        """New a counter and return its uniq id.
        Args:
            to (int): seconds to go.
        Returns:
            str: counter's uniq id.
        """
        end_num = request.args.get('to')
        new_counter = counter_manager.new_counter(end_num)
        if new_counter is not None:
            resp = make_response(new_counter.uid, 200)
            resp.mimetype = 'text/plain'
            return resp
        return ('must have a querystring: [to] '
                'and the value should be positive integer.'), 400

class CounterStopApi(Resource):
    """CounterStopApi is a restful request handler
           to stop an existed counter.
       Provide following methods:
           POST - stop an existed counter.
    """
    def post(self, uid):
        """To stop a counter.
        Args:
            uid (str): stop the counter by uid.
        """
        counter_manager.remove_counter(uid)
        return 'ok', 200
