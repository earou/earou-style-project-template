# -*- coding: utf-8 -*-
"""serviceinfo

This module define a class to response service infos for a restful api call.
"""
import socket
import os
from flask_restful import Resource

class ServiceInfo(Resource):
    """ServiceInfo is a restful request handler.
       Provide following methods:
           GET - return info of the service. hostname and version.
                 Example: host1 version: 0.0.1
    """
    def get(self):
        """return hostname and version info.
        """
        version_str = 'dev'
        try:
            cwd = os.getcwd()
            ver_file = open(cwd+'/version', 'r')
            version_str = ver_file.read()
        except IOError:
            pass
        return socket.gethostname() + ' ver: ' + version_str.rstrip(), 200
