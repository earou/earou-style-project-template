# -*- coding: utf-8 -*-
"""counter
This module include a model class, Counter.
"""
import uuid
import time
class Counter(object):
    """Model class of counter api.
    Args:
        end_num (int): Specify this counter will finish after end_num seconds.
        uid (str, optional): Uniq id(UUID) for a counter instance.
        start_ts (int, optional): When this counter was created.

    Attributes:
        end_num (int): Specify this counter will finish after end_num seconds.
        uid (str): Uniq id(UUID) for a counter instance.
        start_time_in_sec (int): When this counter was created, unit: secound.
        end_time_in_sec (int): When this counter will finish,
            calculated from end_num and start_time_in_sec.
    """
    def __init__(self,
                 end_num,
                 uid=None,
                 start_ts=None):
        self.uid = uid if uid else str(uuid.uuid4()).upper()
        self.end_num = int(end_num)
        if self.end_num < 1:
            raise ValueError('end_num should be positive integer.')
        self.start_time_in_sec = start_ts if start_ts else int(time.time())
        self.end_time_in_sec = self.end_num + self.start_time_in_sec
    def get_current_num(self):
        """Current count number.
        Returns:
            int: Current count number. if times is up, return end number.
        """
        diff = self.end_time_in_sec - int(time.time())
        if diff < 0:
            return self.end_num
        return self.end_num - diff
    def is_finished(self):
        """Check if time is up.
        Returns:
            bool: Time is up or not.
        """
        return self.end_time_in_sec <= int(time.time())
