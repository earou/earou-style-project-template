# -*- coding: utf-8 -*-
"""abcstore
This module include a abstract class, CounterStorage.
Providing a gerneral interface of data persistence.
"""
class CounterStorage(object):
    """CounterStorage is a gerneral interface of data persistence.
    """
    def list_counter(self):
        """abstract method"""
        pass
    def load_counter(self, uid):
        """abstract method"""
        pass
    def save_counter(self, counter):
        """abstract method"""
        pass
    def remove_counter(self, uid):
        """abstract method"""
        pass
