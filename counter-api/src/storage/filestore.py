# -*- coding: utf-8 -*-
"""filestore
This module include a data persistence proxy class, CounterFileStorage.
"""
import os
import json
from model.counter import Counter
from storage.abcstore import CounterStorage
class CounterFileStorage(CounterStorage):
    """CounterFileStorage class implements interface of CounterStorage.
    Attributes:
        counters (dict): A dict of counter instances.
    """
    BASE_DIR = 'counter-data'
    def __init__(self):
        if not os.path.isdir(CounterFileStorage.BASE_DIR):
            os.mkdir(CounterFileStorage.BASE_DIR)
    def list_counter(self):
        """list counters from data storage.
        Returns:
           list: uid list.
        """
        try:
            file_list = os.listdir(CounterFileStorage.BASE_DIR)
            for index, filename in enumerate(file_list):
                file_list[index] = filename[0:-5]
            return file_list
        except OSError:
            return []

    def load_counter(self, uid):
        """Load counter from data storage.
        Args:
            uid (str): load counter by uid.
        Returns:
            Counter: If there is no data from
                data storage, return None.
        """
        counter_file = CounterFileStorage.BASE_DIR + '/' + uid + '.json'
        if not os.path.isfile(counter_file):
            return None
        with open(counter_file) as json_file:
            json_data = json.load(json_file)
            loaded_counter = Counter(json_data['end_num'],
                                     json_data['uid'],
                                     json_data['start_time'])
            return loaded_counter

    def save_counter(self, counter):
        """Save counter to data storage.
        Args:
            counter (Counter): counter to save.
        """
        json_data = {}
        json_data['uid'] = counter.uid
        json_data['end_num'] = counter.end_num
        json_data['start_time'] = counter.start_time_in_sec
        counter_file = CounterFileStorage.BASE_DIR + '/' + counter.uid + '.json'
        with open(counter_file, 'w') as outfile:
            json.dump(json_data, outfile)

    def remove_counter(self, uid):
        """Remove counter data storage.
        Args:
            uid (str): remove the counter by uid.
        """
        counter_file = CounterFileStorage.BASE_DIR + '/' + uid + '.json'
        if os.path.isfile(counter_file):
            os.remove(counter_file)
