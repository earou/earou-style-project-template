"""unit test for model.counter"""

import unittest
import time
from model.counter import Counter

class TestCounterMethods(unittest.TestCase):
    """test suite"""
    def test_uuid(self):
        """test two counter instances have different uuid."""
        ## setup
        counter1 = Counter(100)
        counter2 = Counter(100)
        ## test
        # test counters's uid are different.
        self.assertFalse(counter1.uid == counter2.uid)

    def test_load_counter(self):
        """test init counter by loading."""
        ## setup
        old_counter = Counter(1,
                              '718a79d4-1b0a-11e9-9f39-784f43a5dffe',
                              1547806962)
        ## test
        # counter should be finish because start time is in the past.
        self.assertTrue(old_counter.is_finished())
        # uid should be the same as I set.
        self.assertEqual(old_counter.uid,
                         '718a79d4-1b0a-11e9-9f39-784f43a5dffe')
        # current num should be the same with end num.
        self.assertEqual(old_counter.get_current_num(), old_counter.end_num)

    def test_counting(self):
        """test counting."""
        ## setup
        new_counter = Counter(3)
        ## test
        # counter shouldn't be finish because still 3 secs to go.
        self.assertFalse(new_counter.is_finished())
        # get count as ts1.
        ts1 = new_counter.get_current_num()
        time.sleep(1)
        # get count as ts2 after 1 sec later.
        ts2 = new_counter.get_current_num()
        # ts2 - ts1 = 1.
        self.assertEqual(ts2-ts1, 1)
        # wait for time up.
        time.sleep(2)
        # counter should be finish after time up.
        self.assertTrue(new_counter.is_finished())

    def test_bad_arg(self):
        """test init counter with bad arg."""
        ## test
        # test init with negative number
        with self.assertRaises(ValueError):
            Counter(-3)
        # test init with str
        with self.assertRaises(ValueError):
            Counter('str')
        # test str of int
        counter = Counter('10')
        self.assertEqual(counter.end_num, 10)

if __name__ == '__main__':
    unittest.main()
