"""unit test for control.countermanager"""

import unittest
from control.countermanager import counter_manager
from storage.abcstore import CounterStorage
class TestCounterManagerMethods(unittest.TestCase):
    """test suite"""
    def test_counter_funcs(self):
        """test stored counters will be return as a list."""
        ## setup
        counter_manager.storage_proxy = CounterStorage()
        ## test
        counter1 = counter_manager.new_counter(100)
        # test create counter correctly
        self.assertEqual(counter1.end_num, 100)
        counter2 = counter_manager.get_counter(counter1.uid)
        # test get counter correctly
        self.assertEqual(counter1.uid, counter2.uid)
        # test remove counter correctly
        counter_manager.remove_counter(counter1.uid)
        counter3 = counter_manager.get_counter(counter1.uid)
        self.assertEqual(counter3, None)

if __name__ == '__main__':
    unittest.main()
