"""unit test for storage.filestore"""

import unittest
import os
import shutil
from storage.filestore import CounterFileStorage
class TestFileStorageMethods(unittest.TestCase):
    """test suite"""
    def test_list_counter(self):
        """test stored counters will be return as a list."""
        ## setup
        uuid1 = 'C2B1AAF0-076C-49F7-AA34-233B5B2B72BC'
        uuid2 = '7F93FDFF-635A-45E1-A4EF-848E27899F8E'
        uuid3 = '442B8293-E090-4DF2-B1B7-ABDAC726B5D0'
        basedir = 'counter-data'
        if os.path.isdir(basedir):
            shutil.rmtree(basedir)
        os.mkdir(basedir)
        open(basedir + '/' + uuid1 + '.json', 'a').close()
        open(basedir + '/' + uuid2 + '.json', 'a').close()
        open(basedir + '/' + uuid3 + '.json', 'a').close()
        ## test
        # test returned list equal to files
        storage_proxy = CounterFileStorage()
        counter_list = storage_proxy.list_counter()
        self.assertItemsEqual(counter_list, [uuid1, uuid2, uuid3])
        ## cleanup
        shutil.rmtree(basedir)

if __name__ == '__main__':
    unittest.main()
