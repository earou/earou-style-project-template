"""unit test for handler.serviceinfo"""

import unittest
import os
from handler.serviceinfo import ServiceInfo

class TestServiceInfoMethods(unittest.TestCase):
    """test suite"""
    def test_get_without_ver(self):
        """test GET without version file"""
        # test
        handler = ServiceInfo()
        (msg, status_code) = handler.get()
        self.assertEqual(status_code, 200)
        self.assertIn('ver: dev', msg)

    def test_get_with_ver(self):
        """test GET with version file"""
        # setup
        cwd = os.getcwd()
        filepath = cwd+'/version'
        ver_file = open(filepath, 'w')
        ver_file.write("1.0.0")
        ver_file.close()
        # test
        handler = ServiceInfo()
        (msg, status_code) = handler.get()
        self.assertEqual(status_code, 200)
        self.assertIn('ver: 1.0.0', msg)
        # cleanup
        os.remove(filepath)


if __name__ == '__main__':
    unittest.main()
