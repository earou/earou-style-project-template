#!/bin/bash
set -e

# General Configs
COUNTER_QTY=$1
COUNTER_TO=$2
NGX_IP=127.0.0.1
# Constants
BASEDIR=$(dirname "$0")
VERSION=$(cat ${BASEDIR}/version)
# Doc
function arg_doc {
  echo -e "\033[1;37mVersion:\033[m ${VERSION}"
  echo ""
  echo -e "\033[1;37mDescription:\033[m"
  echo "    call restful api to create amount of counters."
  echo ""
  echo -e "\033[1;37mUsage: $0 <counter-qty> <counter-to>\033[m"
  echo "  * (optional)counter-qty: how many counters to create."
  echo "  * (optional)counter-to: counters will expire after what seconds."
  echo ""
  echo -e "\033[1;37mExamples:\033[m"
  echo "   1. create 100 counters: $0 "
  echo "   2. create 7 counters expired after 10 secs: $0 7 10"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi
# Arg Validation
if ! [[ "$COUNTER_QTY" =~ ^[0-9]+$ ]]; then
  COUNTER_QTY=100
  echo "<counter-qty> is not specify or not valid. using default value: 100"
fi
if ! [[ "$COUNTER_TO" =~ ^[0-9]+$ ]]; then
  COUNTER_TO=1000
  echo "<counter-to> is not specify or not valid. using default value: 1000"
fi

if [ "$COUNTER_QTY" -lt 1 ]; then
    echo -e "\033[1;31m<counter-qty> must be positive integer.(> 0)\033[m"
    exit 1
fi
# Main Scripts
for i in $(seq 1 $COUNTER_QTY);
do
    curl -s -XPOST http://${NGX_IP}/counter/\?to\=${COUNTER_TO} > /dev/null
done
echo -e "\033[1;37m  *  Create ${COUNTER_QTY} counters succeeded!!\033[m"
