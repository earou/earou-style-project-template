#!/bin/bash
set -e

# Args
BUILD_ID=$1
# General Configs
REPO_NAME='earou/api-counter'
# Contants
BASEDIR=$(dirname "$0")
PROJECT_ROOT=${BASEDIR}/../
# Doc
function arg_doc {
  echo -e "\033[1;37mDescription:\033[m"
  echo "    build counter-api docker images and pack operation scripts."
  echo ""
  echo -e "\033[1;37mUsage: $0 <build-id> \033[m"
  echo "  * (optional)build-id: version_YYYYmmDD-HHMM"
  echo ""
  echo -e "\033[1;37mExamples:\033[m"
  echo "   1. using current datetime and version: $0"
  echo "   2. specify build-id: $0 rel-1.0.0"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi
# Arg Validation
if [ "${BUILD_ID}" == "" ]; then
  var_ts=`date +%Y%m%d-%H%M`
  var_ver=$(cat $PROJECT_ROOT/version)
  BUILD_ID=${var_ver}_${var_ts}
  echo -e "     <build-id> is not specified. auto generated: \033[1;37m${BUILD_ID}\033[m"
fi

# Main Scripts
echo -e "\033[1;37m  *  Start to build all artifacts in this project:\033[m (total 5 steps)"

echo "       1. create artifact dir...(1/5)"
mkdir -p ${PROJECT_ROOT}/artifacts/${BUILD_ID}
ls ${PROJECT_ROOT}/artifacts/${BUILD_ID}

echo "       2. create LATEST.txt file and soft link to latest artifact dir...(2/5)"
cd ${PROJECT_ROOT}/artifacts
echo ${BUILD_ID} > LATEST.txt
ls LATEST.txt
[ -e latest ] && rm latest
ln -s ${BUILD_ID} latest
cd ${PROJECT_ROOT}/pipeline

echo "       3. build docker image of ${REPO_NAME}...(3/5)"
cd ${PROJECT_ROOT}/counter-api
./02_build.sh ${BUILD_ID}
cd ${PROJECT_ROOT}/pipeline

echo "       4. copy operation scripts...(4/5)"
echo ${BUILD_ID} > ${PROJECT_ROOT}/artifacts/${BUILD_ID}/version
cp ${PROJECT_ROOT}/setup_api.sh ${PROJECT_ROOT}/artifacts/${BUILD_ID}/
cp ${PROJECT_ROOT}/terminate_api.sh ${PROJECT_ROOT}/artifacts/${BUILD_ID}/
cp ${PROJECT_ROOT}/list_counter.sh ${PROJECT_ROOT}/artifacts/${BUILD_ID}/
cp ${PROJECT_ROOT}/create_counter.sh ${PROJECT_ROOT}/artifacts/${BUILD_ID}/
cp ${PROJECT_ROOT}/wait_counter.sh ${PROJECT_ROOT}/artifacts/${BUILD_ID}/
cp -r ${PROJECT_ROOT}/ngx ${PROJECT_ROOT}/artifacts/${BUILD_ID}/

echo "       5. pack operation scripts...(5/5)"
cd ${PROJECT_ROOT}/artifacts
EXT_NAME="tar.gz"
tar zcf ${BUILD_ID}.${EXT_NAME} ${BUILD_ID}
cd ${PROJECT_ROOT}/pipeline

echo -e "\033[1;37m  *  Build process succeeded!!\033[m"
