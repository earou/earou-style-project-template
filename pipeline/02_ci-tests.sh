#!/bin/bash
set -e

# Args
BUILD_ID=$1
# General Configs
REPO_NAME='earou/api-counter'
# Contants
BASEDIR=$(dirname "$0")
PROJECT_ROOT=${BASEDIR}/../
# Doc
function arg_doc {
  echo -e "\033[1;37mDescription:\033[m"
  echo "    run all tests in this project."
  echo ""
  echo -e "\033[1;37mUsage: $0 <build-id> \033[m"
  echo "  * (optional)build-id: version_YYYYmmDD-HHMM"
  echo ""
  echo -e "\033[1;37mExamples:\033[m"
  echo "   1. using latest build version: $0"
  echo "   2. specify build-id: $0 rel-1.0.0"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi
# Arg Validation
if [ "${BUILD_ID}" == "" ]; then
  if [ ! -e $PROJECT_ROOT/artifacts/LATEST.txt ]; then
    echo -e "\033[1;31m   latest build doesn't exist. please run ./01_build-artifact.sh ahead.\033[0m"
    exit 1
  fi
  BUILD_ID=$(cat $PROJECT_ROOT/artifacts/LATEST.txt)
  echo -e "     <build-id> is not specified. using latest: \033[1;37m${BUILD_ID}\033[m"
fi

# Main Scripts
echo -e "\033[1;37m  *  Start to run ci tests in this project:\033[m (total 2 steps)"

echo "       1. run tests in python project: counter-api...(1/2)"
cd ${PROJECT_ROOT}/counter-api
./01_test.sh
cd ${PROJECT_ROOT}/pipeline

echo "       2. run integration tests...(2/2)"
cd ${PROJECT_ROOT}/tests
./run_tests.sh ${BUILD_ID}
cd ${PROJECT_ROOT}/pipeline

echo -e "\033[1;37m  *  All ci tests succeeded!!\033[m"