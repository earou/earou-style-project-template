#!/bin/bash
set -e

# Args
BACKEND_REPLICA_QTY=$1
BACKEND_VERSION=$2
# General Configs
INSTANCE_PREFIX='api-counter'
BACKEND_REPO='earou/api-counter'
BACKEND_LISTEN=5000
# Constants
BASEDIR=$(dirname "$0")
VERSION=$(cat ${BASEDIR}/version)
UPSTREAM_NGX_CONF="${BASEDIR}/ngx/upstreams.conf"
# Doc
function arg_doc {
  echo -e "\033[1;37mVersion:\033[m ${VERSION}"
  echo ""
  echo -e "\033[1;37mDescription:\033[m"
  echo "    setup a proxy with specific quantity of backend containers."
  echo ""
  echo -e "\033[1;37mUsage: $0 <replica-qty> <backend-version>\033[m"
  echo "  * replica-qty: quantity of backend instances."
  echo "  * (optional)backend-version: tag of backend image. default: latest."
  echo ""
  echo -e "\033[1;37mExamples:\033[m"
  echo "   1. setup a proxy with 3 backend containers: $0 3"
  echo "   2. specify backend version: $0 3 0.0.1-step1_20190118-1312"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi
# Arg Validation
if ! [[ "$BACKEND_REPLICA_QTY" =~ ^[0-9]+$ ]]; then
  arg_doc
  echo -e "\033[1;31mreplica-qty is missing or not valid.\033[m"
  exit 1
fi
if [ "${BACKEND_VERSION}" == "" ]; then
  BACKEND_VERSION=$VERSION
  var_image_exist=$(docker images -q $BACKEND_REPO:$BACKEND_VERSION | wc -l)
  if [ $var_image_exist -lt 1 ]; then
    BACKEND_VERSION='latest'
  fi
fi

function organise_backend {
  var_desire_qty=$1
  var_curr_qty=$(docker ps | grep "$BACKEND_REPO" | wc -l)
  echo "           current qty: ${var_curr_qty}, desire qty: ${var_desire_qty}"

  if [ $var_desire_qty -gt $var_curr_qty ]; then
    var_diff=`expr $var_desire_qty - $var_curr_qty`
    echo "           start to add $var_diff instances."
    var_start_idx=`expr $var_curr_qty + 1`
    provision_backend $var_start_idx $var_desire_qty
  elif [ $var_desire_qty -lt $var_curr_qty ]; then
    var_diff=`expr $var_curr_qty - $var_desire_qty`
    echo "           start to terminate $var_diff instances."
    var_start_idx=`expr $var_desire_qty + 1`
    terminate_backend $var_start_idx $var_curr_qty
  else
    echo "           do nothing."
    exit 0
  fi
}

function provision_backend {
  var_start=$1
  var_end=$2
  if [ $var_end -lt $var_start ]; then
    echo "\033[1;31m           parameter err.\033[m"
    exit 1
  fi
  for i in $(seq $var_start $var_end);
  do
    var_instance_name=${INSTANCE_PREFIX}-${i}
    echo "           add ${var_instance_name}"
    docker run -d --name ${var_instance_name} -v $(pwd)/counter-data:/app/counter-data --hostname host${i} ${BACKEND_REPO}:${BACKEND_VERSION} > /dev/null
  done
}

function terminate_backend {
  var_start=$1
  var_end=$2
  if [ $var_end -lt $var_start ]; then
    echo "\033[1;31m           parameter err.\033[m"
    exit 1
  fi
  for i in $(seq $var_start $var_end);
  do
    var_instance_name=${INSTANCE_PREFIX}-${i}
    echo "           rm $var_instance_name"
    docker kill $var_instance_name > /dev/null
    docker rm $var_instance_name > /dev/null
  done
}

function gen_nginx_upstream {
  var_end=$1
  var_upstream_port=$2
  var_conf_file=$3
  echo "upstream counter_service {" > $var_conf_file
  if [ $var_end -gt 0 ]; then 
    for i in $(seq 1 $var_end);
    do
      var_upstream_ip=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" ${INSTANCE_PREFIX}-${i})
      echo "     server ${var_upstream_ip}:${var_upstream_port};" >> $var_conf_file
    done
  else
    echo "     server 172.17.0.2:5000;" >> $var_conf_file
  fi
  echo "}" >> $var_conf_file
}

function restart_proxy {
  var_instance_name=${INSTANCE_PREFIX}-proxy
  var_is_running=$(docker ps | grep $var_instance_name | wc -l)
  if [ $var_is_running -gt 0 ]; then
    echo "           stop $var_instance_name"
    docker kill $var_instance_name > /dev/null
    docker rm $var_instance_name > /dev/null
  fi
  echo "           start $var_instance_name"
  docker run -p 80:80 --name ${var_instance_name} -v $(pwd)/ngx:/etc/nginx/conf.d:ro -d nginx:latest > /dev/null
}

# Main Scripts
echo -e "\033[1;37m  *  Start to setup ${INSTANCE_PREFIX}(v${VERSION}):\033[m (total 3 steps)"

echo "       1. organise backend replica qty to: ${BACKEND_REPLICA_QTY}, image: ${BACKEND_REPO}:${BACKEND_VERSION}...(1/3)"
organise_backend $BACKEND_REPLICA_QTY

echo "       2. update upstream list of proxy($UPSTREAM_NGX_CONF)...(2/3)"
gen_nginx_upstream $BACKEND_REPLICA_QTY $BACKEND_LISTEN $UPSTREAM_NGX_CONF

echo "       3. restart a nginx instance as proxy...(3/3)"
restart_proxy

echo -e "\033[1;37m  *  Setup ${INSTANCE_PREFIX} succeeded!!\033[m"
echo -e "       try to visit: \033[1;37mhttp://127.0.0.1/\033[m"