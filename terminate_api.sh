#!/bin/bash
set -e

# General Configs
INSTANCE_PREFIX='api-counter'
# Constants
BASEDIR=$(dirname "$0")
VERSION=$(cat ${BASEDIR}/version)
# Doc
function arg_doc {
  echo -e "\033[1;37mVersion:\033[m ${VERSION}"
  echo ""
  echo -e "\033[1;37mDescription:\033[m"
  echo "    terminate all of resources created by setup_api.sh."
  echo ""
  echo -e "\033[1;37mUsage: $0 \033[m"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi

# Main Scripts
echo -e "\033[1;37m  *  Start to terminate ${INSTANCE_PREFIX}(v${VERSION}):\033[m (total 5 steps)"

echo "       1. check running instances...(1/4)"
var_names=$(docker ps --format '{{.Names}}' | grep "^${INSTANCE_PREFIX}-" | awk '{print $1}' | xargs)
if [ "$var_names" == "" ]; then
  echo -e "\033[1;31m           there is no runnning instances.\033[0m"
  echo ""
  exit 1
fi

echo "       2. kill running instances...(2/5)"
docker kill $var_names > /dev/null

echo "       3. remove containers from docker daemon...(3/5)"
docker rm $var_names > /dev/null

echo "       4. remove generated upstream conf...(4/5)"
rm ngx/upstreams.conf

echo "       5. remove counter-data...(5/5)"
sudo rm -r counter-data

echo -e "\033[1;37m  *  Terminate ${INSTANCE_PREFIX} succeeded!!\033[m"