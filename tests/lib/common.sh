#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh

function assertFileContainStr {
  var_file=$1
  var_str=$2
  var_tmp_result=$(cat $var_file | grep "$var_str" -c)
  if [ $var_tmp_result == 1 ]
  then
    printMsg "assertFileContainStr: $var_file passed."
  else
    printFailure "assertFileContainStr: $var_file failed."
    export TEST_RESULT=1
  fi
}

function assertContainStr {
  var_str1=$1
  var_str2=$2
  var_tmp_result=$(echo "$var_str1" | grep "$var_str2" -c)
  if [ $var_tmp_result == 1 ]
  then
    printMsg "assertContainStr passed."
  else
    printFailure "assertContainStr failed."
    export TEST_RESULT=1
  fi
}

function assertEqual {
  var_str1=$1
  var_str2=$2
  if [ "$var_str1" == "$var_str2" ]
  then
    printMsg "assertEqual passed."
  else
    printFailure "assertEqual failed."
    export TEST_RESULT=1
  fi
}

function assertRegex {
  var_reg=$1
  var_str=$2
  if [[ "$var_str" =~ $var_reg ]];
  then
    printMsg "assertRegex passed."
  else
    printFailure "assertRegex failed."
    export TEST_RESULT=1
  fi
}

function makeFail {
  var_msg=$1
  printFailure "$var_msg"
  export TEST_RESULT=1
}