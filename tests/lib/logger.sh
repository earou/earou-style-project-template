#!/bin/bash

function printMsg {
  LIGHT_RED='\033[1;31m'
  NC='\033[0m'
  var_msg=$1
  echo -e "          ${var_msg}"
}

function printTitle {
  LIGHT_RED='\033[1;31m'
  NC='\033[0m'
  var_msg=$1
  echo -e "      ${var_msg}"
}

function printFailure {
  LIGHT_RED='\033[1;31m'
  NC='\033[0m'
  var_msg=$1
  echo -e "${LIGHT_RED}          ${var_msg}${NC}"
}

function printHighlight {
  LIGHT_WHITE='\033[1;37m'
  NC='\033[0m'
  var_msg=$1
  echo -e "${LIGHT_WHITE}  *  ${var_msg}${NC}"
}

function linebreak {
  echo ""
}
