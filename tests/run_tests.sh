#!/bin/bash

# Args
BUILD_ID=$1
# General Configs
REPO_NAME='earou/api-counter'

# Contants
BASEDIR=$(dirname "$0")
PROJECT_ROOT=${BASEDIR}/../
TC_PATH=${BASEDIR}/testcases
# Libs
source ${BASEDIR}/lib/logger.sh

# Doc
function arg_doc {
  echo -e "\033[1;37mDescription:\033[m"
  echo "    run integration tests in this project."
  echo ""
  echo -e "\033[1;37mUsage: $0 <build-id> \033[m"
  echo "  * (optional)build-id: version_YYYYmmDD-HHMM"
  echo ""
  echo -e "\033[1;37mExamples:\033[m"
  echo "   1. run instantly from source code: $0"
  echo "   2. specify build-id: $0 rel-1.0.0"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi
# Arg Validation
if [ "${BUILD_ID}" == "" ]; then
  BUILD_ID=latest
fi
ARTIFACT_PATH=${PROJECT_ROOT}/artifacts/${BUILD_ID}
if [ ! -d $ARTIFACT_PATH ]; then
  printFailure "$ARTIFACT_PATH doesn't exist. using project root."
  ARTIFACT_PATH=$PROJECT_ROOT
fi

TEST_RESULT=0
function main {
  printHighlight "Start integration test($ARTIFACT_PATH):"
  while IFS='' read -r line || [[ -n "$line" ]]; do
      linebreak;
      # if [ "${line}" != "lib/" ]; then
      printHighlight "exec: ${line}test.sh"
      ./${line}test.sh "${line%?}" $ARTIFACT_PATH
      if [ $? -gt 0 ]; then
        TEST_RESULT=1
      fi
      # fi
  done < <(ls -1d $TC_PATH/*/)
  linebreak;
  if [ $TEST_RESULT -gt 0 ]; then
    printFailure "Integration test failed."
    exit 1
  else
    printHighlight "Integration test succeeded."
    exit 0
  fi
  linebreak;
}

main