#!/bin/bash
# Contants
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test setup_api.sh with arguments
TC=$1
TARGET_PATH=$2
TEST_RESULT=0

function setupTC {
  printTitle "setup ${TC}"
}

function runTC {
  printTitle "test ${TC}"

  printMsg "test setup_api.sh without argument."
  var_result=$($TARGET_PATH/setup_api.sh)
  assertContainStr "$var_result" 'replica-qty is missing or not valid'

  printMsg "test setup_api.sh with negative integer."
  var_result=$($TARGET_PATH/setup_api.sh -1)
  assertContainStr "$var_result" 'replica-qty is missing or not valid'

  printMsg "test setup_api.sh with float number."
  var_result=$($TARGET_PATH/setup_api.sh -1)
  assertContainStr "$var_result" 'replica-qty is missing or not valid'

  printMsg "test setup_api.sh with string."
  var_result=$($TARGET_PATH/setup_api.sh text)
  assertContainStr "$var_result" 'replica-qty is missing or not valid'

  printMsg "test setup_api.sh with -h option."
  var_result=$($TARGET_PATH/setup_api.sh -h)
  assertContainStr "$var_result" 'Usage:'
}

function cleanTC {
  printTitle "clean ${TC}"
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
