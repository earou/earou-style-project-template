#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test terminate_api.sh with arguments
TC=$1
TARGET_PATH=$2
TEST_RESULT=0

function setupTC {
  printTitle "setup ${TC}"
}

function runTC {
  printTitle "test ${TC}"

  printMsg "test terminate_api.sh with -h option."
  var_result=$($TARGET_PATH/terminate_api.sh -h)
  assertContainStr "$var_result" 'Usage:'

  printMsg "test terminate_api.sh with no running instances."
  var_result=$($TARGET_PATH/terminate_api.sh)
  assertContainStr "$var_result" 'there is no runnning instances'
}

function cleanTC {
  printTitle "clean ${TC}"
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
