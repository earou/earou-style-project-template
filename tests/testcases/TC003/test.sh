#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test setup_api.sh behavior and proxy functions.
TC=$1
TARGET_PATH=$2
TEST_RESULT=0
CUR_DIR=$(pwd)

function setupTC {
  printTitle "setup ${TC}"
  cd $TARGET_PATH
  ./setup_api.sh 3
  cd $CUR_DIR
}

function runTC {
  printTitle "test ${TC}"

  printMsg "test api-counter instances quantity."
  var_result=$(docker ps | grep earou/api-counter | wc -l)
  assertEqual $var_result 3

  printMsg "test proxy instance quantity."
  var_result=$(docker ps | grep api-counter-proxy | wc -l)
  assertEqual $var_result 1

  printMsg "test proxy load balancing to backend."
  var_result=$(for x in `seq 1 10`; do curl -s http://127.0.0.1/ ; done | sort | uniq | xargs)
  assertContainStr "$var_result" 'host1'
  assertContainStr "$var_result" 'host2'
  assertContainStr "$var_result" 'host3'
}

function cleanTC {
  printTitle "clean ${TC}"
  cd $TARGET_PATH
  ./terminate_api.sh
  cd $CUR_DIR
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
