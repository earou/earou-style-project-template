#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test end2end function of counter api
TC=$1
TARGET_PATH=$2
TEST_RESULT=0
CUR_DIR=$(pwd)

function setupTC {
  printTitle "setup ${TC}"
  cd $TARGET_PATH
  ./setup_api.sh 3
  cd $CUR_DIR
}

function runTC {
  printTitle "test ${TC}"

  printMsg "test POST a new counter."
  var_counter_uuid=$(curl -s -X POST http://127.0.0.1/counter/\?to\=1000)
  var_uuid_regex='[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}'
  assertRegex "$var_uuid_regex" "$var_counter_uuid" 
  printMsg "test GET an existed counter."
  var_json=$(curl -s http://127.0.0.1/counter/${var_counter_uuid}/)
  assertContainStr "$var_json" 'current'
  var_count1=$(echo "$var_json" | docker run -i stedolan/jq '.current')

  printMsg "test counter num after 1 second."
  var_json=$(sleep 1 && curl -s http://127.0.0.1/counter/${var_counter_uuid}/)
  var_count2=$(echo "$var_json" | docker run -i stedolan/jq '.current')
  if [ ! $var_count2 -gt $var_count1 ]; then
    makeFail "counter does not work."
  fi
}

function cleanTC {
  printTitle "clean ${TC}"
  cd $TARGET_PATH
  ./terminate_api.sh
  cd $CUR_DIR
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
