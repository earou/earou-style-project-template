#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test list function of counter api
TC=$1
TARGET_PATH=$2
TEST_RESULT=0
CUR_DIR=$(pwd)

function setupTC {
  printTitle "setup ${TC}"
  cd $TARGET_PATH
  ./setup_api.sh 3
  cd $CUR_DIR
}

function runTC {
  printTitle "test ${TC}"

  printMsg "create 100 counters."
  cd $TARGET_PATH
  ./create_counter.sh
  printMsg "list counters."
  var_qty=$(./list_counter.sh | wc -l)
  assertEqual $var_qty 100
  cd $CUR_DIR
}

function cleanTC {
  printTitle "clean ${TC}"
  cd $TARGET_PATH
  ./terminate_api.sh
  cd $CUR_DIR
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
