#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test dynamically change backend number of setup_api.sh
TC=$1
TARGET_PATH=$2
TEST_RESULT=0
CUR_DIR=$(pwd)

function setupTC {
  printTitle "setup ${TC}"
  cd $TARGET_PATH
  ./setup_api.sh 3
  cd $CUR_DIR
}

function runTC {
  printTitle "test ${TC}"

  cd $TARGET_PATH
  printMsg "create 100 counters."
  ./create_counter.sh

  printMsg "change the number of backend to 0."
  ./setup_api.sh 0
  var_resp=$(curl -s http://127.0.0.1/)
  assertContainStr "$var_resp" '503 Service Unavailable'

  printMsg "change the number of backend to 5."
  ./setup_api.sh 5

  printMsg "test backend hosts."
  var_result=$(for x in `seq 1 10`; do curl -s http://127.0.0.1/ ; done | sort | uniq | xargs)
  assertContainStr "$var_result" 'host1'
  assertContainStr "$var_result" 'host2'
  assertContainStr "$var_result" 'host3'
  assertContainStr "$var_result" 'host4'
  assertContainStr "$var_result" 'host5'

  printMsg "test counter list is still available."
  var_qty=$(./list_counter.sh | wc -l)
  assertEqual $var_qty 100
  cd $CUR_DIR
}

function cleanTC {
  printTitle "clean ${TC}"
  cd $TARGET_PATH
  ./terminate_api.sh
  cd $CUR_DIR
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
