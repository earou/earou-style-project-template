#!/bin/bash
BASEDIR=$(dirname "$0")
source ${BASEDIR}/../../lib/logger.sh
source ${BASEDIR}/../../lib/common.sh

# Test restful api delete function
TC=$1
TARGET_PATH=$2
TEST_RESULT=0
CUR_DIR=$(pwd)

function setupTC {
  printTitle "setup ${TC}"
  cd $TARGET_PATH
  ./setup_api.sh 3
  cd $CUR_DIR
}

function runTC {
  printTitle "test ${TC}"

  cd $TARGET_PATH
  printMsg "create 10 counters."
  ./create_counter.sh 10

  printMsg "test the num of counters is 10."
  var_qty=$(./list_counter.sh | wc -l)
  assertEqual $var_qty 10

  printMsg "delete all counters."
  for x in $(curl -s http://127.0.0.1/counter/); 
  do 
    curl -s -XPOST http://127.0.0.1/counter/${x}/stop/ > /dev/null
  done

  printMsg "test the num of counters become 0."
  var_qty=$(./list_counter.sh | wc -l)
  assertEqual $var_qty 0
  cd $CUR_DIR
}

function cleanTC {
  printTitle "clean ${TC}"
  cd $TARGET_PATH
  ./terminate_api.sh
  cd $CUR_DIR
}

setupTC
runTC
cleanTC

exit $TEST_RESULT
