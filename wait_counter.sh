#!/bin/bash
set -e

# General Configs
NGX_IP=127.0.0.1
# Constants
BASEDIR=$(dirname "$0")
VERSION=$(cat ${BASEDIR}/version)
# Doc
function arg_doc {
  echo -e "\033[1;37mVersion:\033[m ${VERSION}"
  echo ""
  echo -e "\033[1;37mDescription:\033[m"
  echo "    This script wait for all counters to expire."
  echo -e "\033[1;37mAlgorithm:\033[m"
  echo "    1. get counter list."
  echo "    2. if the size of counter list is 0. Finish."
  echo "    3. get first one counter in the list, to see how long to go."
  echo "    4. sleep until first one counter expired."
  echo "    5. go back to step 3, check next counter"
  echo "       until all counters expired in the list."
  echo "    6. go back to step 1."
  echo ""
  echo -e "\033[1;37mUsage: $0\033[m"
  echo ""
}
# Show Help Doc
if [ "$1" == "-h" ]; then
  arg_doc
  exit 0
fi

# Main Scripts
var_num_counter=1
while [ $var_num_counter -gt 0 ]
do
    var_list=$(curl -s http://${NGX_IP}/counter/)
    var_num_counter=$(echo $var_list | grep '\S' | wc -l)
    for x in $var_list; do
        var_json=$(curl -s http://${NGX_IP}/counter/${x}/)
        var_is_expired=$(echo "$var_json" | grep 'expired' | wc -l)
        if [ $var_is_expired -gt 0 ]; then
            echo "     counter: ${x} expired."
        else
            var_current=$(echo "$var_json" | docker run -i stedolan/jq '.current')
            var_to=$(echo "$var_json" | docker run -i stedolan/jq '.to')
            if [ $var_to -gt $var_current ]; then
                var_to_sleep=`expr $var_to - $var_current`
                echo "     counter: ${x} still $var_to_sleep secs to go."
                echo "     sleep $var_to_sleep"
                sleep $var_to_sleep 
            fi
        fi
    done
done

echo -e "\033[1;37m  *  All counters expired!!\033[m"
